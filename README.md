# Laravel from Scratch
## Page Layout

Steps for structuring views in a layout design pattern:

1. We need to make a structure from the views that we have in the project. For this we can create a layout file.
2. Then we got the unique HTML tags and structure in our layout file, since that code is the reusable layout through the site.
3. Afterwards we can do `@extends ('layout')` for extending or inherite the layout structure to the rest of views.
4.`@yield ('content)` we can use for replacing it with a section of content from a view that were created byb `@section('content') <h1>Hola Mundoo</h1> @endsection.` By this way we are dynamically replacing data in the layout structure.
